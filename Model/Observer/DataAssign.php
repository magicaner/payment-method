<?php
/**
 * @copyright Copyright (c) Mdg, Inc. (https://www.mdggroup.com)
 */
namespace Mdg\PaymentMethod\Model\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Quote\Api\Data\PaymentInterface;

/**
 * Class DataAssign
 *
 * @package Mdg\PaymentMethod\Model
 */
class DataAssign implements ObserverInterface
{
    const PAYMENT_METHOD_NONCE = 'payment_method_nonce';

    /**
     * @var array
     */
    protected $additionalInformationList = [
        self::PAYMENT_METHOD_NONCE,
    ];

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $event = $observer->getEvent();
        $data = $event->getDataByKey(AbstractDataAssignObserver::DATA_CODE);

        $additionalData = $data->getData(PaymentInterface::KEY_ADDITIONAL_DATA);
        if (!is_array($additionalData)) {
            return;
        }

        $paymentInfo = $event->getDataByKey(AbstractDataAssignObserver::MODEL_CODE);

        $paymentInfo->addData($additionalData);
    }
}
