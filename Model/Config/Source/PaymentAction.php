<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2018 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentMethod\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class PaymentAction
 *
 * @package Mdg\PaymentMethod\Model\Config\Source
 */
class PaymentAction implements ArrayInterface
{
    const AUTHORIZE = 'authorize';
    const AUTHORIZE_AND_CAPTURE = 'authorize_capture';

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return [
            ['value' => static::AUTHORIZE, 'label' => __('Charge on Shipment')],
            ['value' => static::AUTHORIZE_AND_CAPTURE, 'label' => __('Charge on Order')],
        ];
    }
}
