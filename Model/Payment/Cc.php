<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2018 Mdg, Inc. (http://www.medzhytov.com)
 */

namespace Mdg\PaymentMethod\Model\Payment;

/**
 * Class Cc
 *
 * @package Mdg\PaymentMethod\Model\Payment
 */
class Cc extends \Magento\Payment\Model\Method\Cc
{
    const CODE = "mdg_cc";

    //@codingStandardsIgnoreLine
    protected $_code = self::CODE;

    //@codingStandardsIgnoreLine
    protected $_isGateway = true;

    //@codingStandardsIgnoreLine
    protected $_canCapture = true;

    //@codingStandardsIgnoreLine
    protected $_canOrder = true;

    //@codingStandardsIgnoreLine
    protected $_canCapturePartial = true;

    //@codingStandardsIgnoreLine
    protected $_canRefund = true;

    //@codingStandardsIgnoreLine
    protected $_canRefundInvoicePartial = true;

    //@codingStandardsIgnoreLine
    protected $_stripeApi = false;

    //@codingStandardsIgnoreLine
    protected $_countryFactory;

    //@codingStandardsIgnoreLine
    protected $_minAmount = null;

    //@codingStandardsIgnoreLine
    protected $_maxAmount = null;

    //@codingStandardsIgnoreLine
    protected $_supportedCurrencyCodes = ['USD'];

    //@codingStandardsIgnoreLine
    protected $_debugReplacePrivateDataKeys
        = ['number', 'exp_month', 'exp_year', 'cvc'];

    /**
     * @var \Mdg\PaymentGateway\Model\GatewayClient
     */
    private $gatewayClient;

    /**
     * Cc constructor.
     *
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger $logger
     * @param \Magento\Framework\Module\ModuleListInterface $moduleList
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Mdg\PaymentGateway\Model\GatewayClient $gatewayClient
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Mdg\PaymentGateway\Model\GatewayClient $gatewayClient,
        array $data = array()
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $moduleList,
            $localeDate,
            null,
            null,
            $data
        );
        $this->_countryFactory = $countryFactory;
        $this->_minAmount = $this->getConfigData('min_order_total');
        $this->_maxAmount = $this->getConfigData('max_order_total');
        $this->gatewayClient = $gatewayClient;
    }

    /**
     * Authorize payment abstract method
     *
     * @param \Magento\Framework\DataObject|InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @api
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function authorize(
        \Magento\Payment\Model\InfoInterface $payment,
        $amount
    ) {
        if (!$this->canAuthorize()) {
            throw new \Magento\Framework\Exception\LocalizedException(__('The authorize action is not available.'));
        }

        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $ccNum = $payment->getData('cc_number');
        $ccHolder = $payment->getOrder()->getBillingAddress()->getName();
        $ccExpMonth = $payment->getData('cc_exp_month');
        $ccExpYear = $payment->getData('cc_exp_year');
        $ccCvv = $payment->getData('cc_cid');
        $amount = $payment->getOrder()->getTotalDue();
        $currency = $payment->getOrder()->getBaseCurrency()->getCode();
        $merchantTnRef = $payment->getOrder()->getIncrementId();
        $comment = "Order " . $payment->getOrder()->getIncrementId();

        $result = $this->gatewayClient->authorize(
            $ccNum,
            $ccHolder,
            $ccExpMonth,
            $ccExpYear,
            $ccCvv,
            $amount,
            $currency,
            $merchantTnRef,
            $comment
        );

        $payment->setTransactionId($result['transaction']['tn_ref']);
        $payment->setIsTransactionClosed(0);

        return $this;
    }

    /**
     * Capture payment abstract method
     *
     * @param \Magento\Framework\DataObject|InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @api
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function capture(
        \Magento\Payment\Model\InfoInterface $payment,
        $amount
    ) {
        if (!$this->canCapture()) {
            throw new \Magento\Framework\Exception\LocalizedException(__('The capture action is not available.'));
        }

        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $ccNum = $payment->getData('cc_number');
        $ccHolder = $payment->getOrder()->getBillingAddress()->getName();
        $ccExpMonth = $payment->getData('cc_exp_month');
        $ccExpYear = $payment->getData('cc_exp_year');
        $ccCvv = $payment->getData('cc_cid');
        $amount = $payment->getOrder()->getTotalDue();
        $currency = $payment->getOrder()->getBaseCurrency()->getCode();
        $merchantTnRef = $payment->getOrder()->getIncrementId();
        $comment = "Order " . $payment->getOrder()->getIncrementId();
        $tnRef = $payment->getLastTransId();

        if ($tnRef) {
            $result = $this->gatewayClient->capture($tnRef, $comment);
        } else {
            $result = $this->gatewayClient->authcapture(
                $ccNum,
                $ccHolder,
                $ccExpMonth,
                $ccExpYear,
                $ccCvv,
                $amount,
                $currency,
                $merchantTnRef,
                $comment
            );
        }


        $payment->setTransactionId($result['transaction']['tn_ref']);
        foreach ($result['transaction'] as $key => $value) {
            $payment->setTransactionAdditionalInfo($key, $value);
        }
        $payment->setIsTransactionClosed(0);

        return $this;
    }

    /**
     * Refund specified amount for payment
     *
     * @param \Magento\Framework\DataObject|InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @api
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function refund(
        \Magento\Payment\Model\InfoInterface $payment,
        $amount
    ) {
        if (!$this->canRefund()) {
            throw new \Magento\Framework\Exception\LocalizedException(__('The refund action is not available.'));
        }

        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $tnRef = $payment->getLastTransId();
        $currency = $payment->getOrder()->getBaseCurrency();

        $result = $this->gatewayClient->refund(
            $tnRef,
            $amount,
            $currency
        );

        $payment->setTransactionId($result['transaction']['tn_ref']);
        foreach ($result['transaction'] as $key => $value) {
            $payment->setTransactionAdditionalInfo($key, $value);
        }
        $payment->setIsTransactionClosed(0);

        return $this;
    }
}
