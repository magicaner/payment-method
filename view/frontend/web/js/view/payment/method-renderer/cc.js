define(
    [
        'Magento_Payment/js/view/payment/cc-form'
    ],
    function (Component) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Mdg_PaymentMethod/payment/cc'
            },
            // add required logic here
            getMailingAddress: function () {
                return window.checkoutConfig.payment.checkmo.mailingAddress;
            },
            context: function() {
                return this;
            },

            getCode: function() {
                return 'mdg_cc';
            },

            isActive: function() {
                return true;
            }
        });
    }
);
