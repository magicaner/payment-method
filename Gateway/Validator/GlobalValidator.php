<?php
/**
 * @copyright Copyright (c) Mdg, Inc. (https://www.mdggroup.com)
 */

namespace Mdg\PaymentMethod\Gateway\Validator;

use Magento\Payment\Gateway\Validator\AbstractValidator;
use Magento\Payment\Gateway\Validator\ValidatorInterface;

/**
 * Class GlobalValidator
 *
 * @package Mdg\PaymentMethod\Gateway\Validator
 */
class GlobalValidator extends AbstractValidator implements ValidatorInterface
{
    /**
     * @inheritdoc
     */
    public function validate(array $validationSubject)
    {
        $isValid = true;
        $storeId = $validationSubject['storeId'];

        return $this->createResult($isValid);
    }
}
