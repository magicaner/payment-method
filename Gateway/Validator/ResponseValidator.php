<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2019 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentMethod\Gateway\Validator;

use Mdg\PaymentMethod\Gateway\Http\Response\Success;

/**
 * Class ResponseValidator
 */
class ResponseValidator extends GeneralResponseValidator
{
    /**
     * @return array
     */
    protected function getResponseValidators()
    {
        return array_merge(
            parent::getResponseValidators(),
            [
                function ($response) {
                    return [
                        $response instanceof Success && $response->getCode() == 0,
                        [__('Wrong transaction status')]
                    ];
                }
            ]
        );
    }
}
