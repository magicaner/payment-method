<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2019 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentMethod\Gateway\Http\Client;

use Mdg\PaymentMethod\Gateway\Request\VoidDataBuilder as DataBuilder;

class TransactionVoid extends \Mdg\PaymentMethod\Gateway\Http\Client\AbstractTransaction
{
    /**
     * Process http request
     * @param array $data
     * @return \Braintree\Result\Error|\Braintree\Result\Successful
     */
    protected function process(array $data)
    {
        $tnRef = $data[DataBuilder::TN_REF];

        $result = $this->gatewayClient->void(
            $tnRef
        );

        return $result;
    }
}
