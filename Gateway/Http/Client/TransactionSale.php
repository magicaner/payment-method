<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2019 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentMethod\Gateway\Http\Client;

use Mdg\PaymentMethod\Gateway\Request\SaleDataBuilder as DataBuilder;

/**
 * Class TransactionSale
 *
 * @package Mdg\PaymentMethod\Gateway\Http\Client
 */
class TransactionSale extends AbstractTransaction
{
    /**
     * Process http request
     * @param array $data
     * @return \Braintree\Result\Error|\Braintree\Result\Successful
     */
    protected function process(array $data)
    {
        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $ccNum = $data[DataBuilder::CC_NUMBER];
        $ccHolder = $data[DataBuilder::CC_HOLDER];
        $ccExpMonth = $data[DataBuilder::CC_EXP_MONTH];
        $ccExpYear = $data[DataBuilder::CC_EXP_YEAR];
        $ccCvv = $data[DataBuilder::CC_CVV];
        $amount = $data[DataBuilder::AMOUNT];
        $currency = $data[DataBuilder::CURRENCY];
        $merchantTnRef = $data[DataBuilder::MERCHANT_TN_REF];
        $comment = $data[DataBuilder::COMMENT];

        $result = $this->gatewayClient->authcapture(
            $ccNum,
            $ccHolder,
            $ccExpMonth,
            $ccExpYear,
            $ccCvv,
            $amount,
            $currency,
            $merchantTnRef,
            $comment
        );

        return $result;
    }
}
