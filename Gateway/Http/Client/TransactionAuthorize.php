<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2019 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentMethod\Gateway\Http\Client;

use Mdg\PaymentMethod\Gateway\Request\AuthorizeDataBuilder;

/**
 * Class TransactionAuthorize
 *
 * @package Magento\Braintree\Gateway\Http\Client
 */
class TransactionAuthorize extends AbstractTransaction
{
    /**
     * Process http request
     * @param array $data
     * @return \Braintree\Result\Error|\Braintree\Result\Successful
     */
    protected function process(array $data)
    {
        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $ccNum = $data[AuthorizeDataBuilder::CC_NUMBER];
        $ccHolder = $data[AuthorizeDataBuilder::CC_HOLDER];
        $ccExpMonth = $data[AuthorizeDataBuilder::CC_EXP_MONTH];
        $ccExpYear = $data[AuthorizeDataBuilder::CC_EXP_YEAR];
        $ccCvv = $data[AuthorizeDataBuilder::CC_CVV];
        $amount = $data[AuthorizeDataBuilder::AMOUNT];
        $currency = $data[AuthorizeDataBuilder::CURRENCY];
        $merchantTnRef = $data[AuthorizeDataBuilder::TN_REF];
        $comment = $data[AuthorizeDataBuilder::COMMENT];

        $result = $this->gatewayClient->authorize(
            $ccNum,
            $ccHolder,
            $ccExpMonth,
            $ccExpYear,
            $ccCvv,
            $amount,
            $currency,
            $merchantTnRef,
            $comment
        );

        return $result;
    }
}
