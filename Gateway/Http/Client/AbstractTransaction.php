<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2019 Mdg, Inc. (http://www.medzhytov.com)
 */

namespace Mdg\PaymentMethod\Gateway\Http\Client;

use Mdg\PaymentMethod\Gateway\Http\Response\Builder;
use Magento\Payment\Gateway\Http\ClientException;
use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use Magento\Payment\Model\Method\Logger;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractTransaction
 *
 * @package Mdg\PaymentMethod\Gateway\Http\Client
 */
abstract class AbstractTransaction implements ClientInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Logger
     */
    protected $customLogger;

    /**
     * @var \Mdg\PaymentGateway\Model\GatewayClient
     */
    protected $gatewayClient;
    /**
     * @var Builder
     */
    private $responseBuilder;

    /**
     * Constructor
     *
     * @param LoggerInterface $logger
     * @param Logger $customLogger
     * @param BraintreeAdapterFactory $adapterFactory
     * @param Builder $builder
     */
    public function __construct(
        LoggerInterface $logger,
        Logger $customLogger,
        \Mdg\PaymentGateway\Model\GatewayClient $gatewayClient,
        Builder $responseBuilder
    ) {
        $this->logger = $logger;
        $this->customLogger = $customLogger;
        $this->gatewayClient = $gatewayClient;
        $this->responseBuilder = $responseBuilder;
    }

    /**
     * @inheritdoc
     */
    public function placeRequest(TransferInterface $transferObject)
    {
        $data = $transferObject->getBody();
        $log = [
            'request' => $data,
            'client' => static::class
        ];
        $response['object'] = null;

        try {
            $responseData = $this->process($data);
            $response['object'] = $this->responseBuilder->create($responseData);
        } catch (\Exception $e) {
            $message = __($e->getMessage() ?: 'Sorry, but something went wrong');
            $this->logger->critical($message);
            throw new ClientException($message);
        }

        return $response;
    }

    /**
     * Process http request
     *
     * @param array $data
     * @return \Braintree\Result\Error|\Braintree\Result\Successful
     */
    abstract protected function process(array $data);
}
