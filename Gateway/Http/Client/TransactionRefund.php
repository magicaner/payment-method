<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2019 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentMethod\Gateway\Http\Client;

use Mdg\PaymentMethod\Gateway\Request\RefundDataBuilder as DataBuilder;

class TransactionRefund extends \Mdg\PaymentMethod\Gateway\Http\Client\AbstractTransaction
{
    /**
     * Process http request
     * @param array $data
     * @return \Braintree\Result\Error|\Braintree\Result\Successful
     */
    protected function process(array $data)
    {
        $currency = $data[DataBuilder::CURRENCY];
        $tnRef = $data[DataBuilder::TN_REF];
        $amount = $data[DataBuilder::AMOUNT];

        $result = $this->gatewayClient->refund(
            $tnRef,
            $amount,
            $currency,
        );

        return $result;
    }
}
