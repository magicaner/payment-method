<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2019 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentMethod\Gateway\Http\Client;

use Mdg\PaymentMethod\Gateway\Request\CaptureDataBuilder as DataBuilder;

class TransactionCapture extends TransactionSale
{
    /**
     * Process http request
     * @param array $data
     * @return \Braintree\Result\Error|\Braintree\Result\Successful
     */
    protected function process(array $data)
    {
        if (!isset($data[DataBuilder::TN_REF])) {
            return parent::process($data);
        }

        $tnRef = $data[DataBuilder::TN_REF];
        $result = $this->gatewayClient->capture(
            $tnRef
        );

        return $result;
    }
}
