<?php
/**
 * @copyright Copyright (c) Mdg, Inc. (https://www.mdggroup.com)
 */
namespace Mdg\PaymentMethod\Gateway\Http\Response;

/**
 * Class Builder
 *
 * @package Mdg\PaymentMethod\Gateway\Http\Response
 */
class Builder
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Builder constructor.
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager)
    {

        $this->objectManager = $objectManager;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        switch ($data['code']) {
            case '0':
                return $this->objectManager
                    ->create(
                        '\Mdg\PaymentMethod\Gateway\Http\Response\Success',
                        ['data' => $data]
                    );
            default:
                return $this->objectManager
                    ->create(
                        '\Mdg\PaymentMethod\Gateway\Http\Response\Error',
                        ['data' => $data]
                    );
        }
    }
}
