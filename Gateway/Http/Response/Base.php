<?php
/**
 * @copyright Copyright (c) Mdg, Inc. (https://www.mdggroup.com)
 */
namespace Mdg\PaymentMethod\Gateway\Http\Response;

use Magento\Framework\DataObject;

/**
 * Class Base
 *
 * @package Mdg\PaymentMethod\Gateway\Http\Response
 */
abstract class Base extends DataObject
{
    /**
     * Base constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->getData('code');
    }
}
