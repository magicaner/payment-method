<?php
/**
 * @copyright Copyright (c) Mdg, Inc. (https://www.mdggroup.com)
 */
namespace Mdg\PaymentMethod\Gateway\Http\Response;

/**
 * Class Error
 *
 * @package Mdg\PaymentMethod\Gateway\Http\Response
 */
class Error extends Base
{

}
