<?php
/**
 * @copyright Copyright (c) Mdg, Inc. (https://www.mdggroup.com)
 */
namespace Mdg\PaymentMethod\Gateway\Http\Response;

/**
 * Class Success
 *
 * @package Mdg\PaymentMethod\Gateway\Http\Response
 */
class Success extends Base
{

}
