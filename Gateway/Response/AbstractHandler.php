<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2019 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentMethod\Gateway\Response;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;

abstract class AbstractHandler implements \Magento\Payment\Gateway\Response\HandlerInterface
{
    /**
     * @param $handlingSubject
     * @return PaymentDataObjectInterface
     */
    protected function getPayment($subject)
    {
        if (!isset($subject['payment'])
            || !$subject['payment'] instanceof PaymentDataObjectInterface
        ) {
            throw new \InvalidArgumentException('Payment data object should be provided');
        }

        $paymentDO = $subject['payment'];

        return $paymentDO;
    }

    /**
     * @inheritdoc
     */
    abstract public function handle(array $handlingSubject, array $response);
}
