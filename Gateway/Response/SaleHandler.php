<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2019 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentMethod\Gateway\Response;

use Magento\Sales\Model\Order\Payment;

/**
 * Class SaleHandler
 *
 * @package Mdg\PaymentMethod\Gateway\Response
 */
class SaleHandler extends AbstractHandler
{
    /**
     * @inheritdoc
     */
    public function handle(array $handlingSubject, array $response)
    {
        $paymentDO = $this->getPayment($handlingSubject);

        $payment = $paymentDO->getPayment();
        $order = $paymentDO->getOrder();
        $result = $response['object']->getData();

        $payment->setTransactionId($result['transaction']['tn_ref']);
        $payment->setIsTransactionClosed(0);
    }
}
