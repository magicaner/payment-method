<?php
/**
 * @copyright Copyright (c) Mdg, Inc. (https://www.mdggroup.com)
 */
namespace Mdg\PaymentMethod\Gateway\Config;

/**
 * Class Config
 *
 * @package Mdg\PaymentMethod\Gateway\Config
 */
class Config extends \Magento\Payment\Gateway\Config\Config
{

}
