<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2018 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentMethod\Gateway\Request;

use Magento\Payment\Gateway\Config\Config;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Payment\Helper\Formatter;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;

/**
 * Class RefundDataBuilder
 *
 * @package Mdg\PaymentMethod\Gateway\Request
 */
class RefundDataBuilder extends PaymentDataBuilder implements BuilderInterface
{
    const CURRENCY = 'currency';
    const TN_REF = 'tnRef';

    /**
     * @inheritdoc
     */
    public function build(array $subject)
    {
        $result = parent::build($subject);

        $paymentDO = $subject['payment'];

        /** @var \Magento\Sales\Model\Order\Payment $payment */
        /** @var \Magento\Sales\Model\Order $order */
        $payment = $paymentDO->getPayment();
        $order = $paymentDO->getOrder();

        $tnRef = $payment->getLastTransId();
        $currency = $payment->getOrder()->getBaseCurrency();

        $result = array_merge($result, [
            self::CURRENCY => $currency,
            self::TN_REF => $tnRef
        ]);

        return $result;
    }
}
