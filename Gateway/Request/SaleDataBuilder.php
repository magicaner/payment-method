<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2018 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentMethod\Gateway\Request;

use Magento\Payment\Gateway\Config\Config;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Payment\Helper\Formatter;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;

/**
 * Class SaleDataBuilder
 *
 * @package Mdg\PaymentMethod\Gateway\Request
 */
class SaleDataBuilder extends PaymentDataBuilder implements BuilderInterface
{
    const AMOUNT = 'amount';
    const CC_NUMBER = 'ccNumber';
    const CC_HOLDER = 'ccHolder';
    const CC_EXP_MONTH = 'ccExpMonth';
    const CC_EXP_YEAR = 'ccExpYear';
    const CC_CVV = 'ccCvv';
    const CURRENCY = 'currency';
    const MERCHANT_TN_REF = 'merchantTnRef';
    const COMMENT = 'comment';

    /**
     * @inheritdoc
     */
    public function build(array $subject)
    {
        $result = parent::build($subject);

        $paymentDO = $subject['payment'];

        /** @var \Magento\Sales\Model\Order\Payment $payment */
        /** @var \Magento\Sales\Model\Order $order */
        $payment = $paymentDO->getPayment();
        $order = $paymentDO->getOrder();

        $ccNum = $payment->getData('cc_number');
        $ccHolder = $payment->getOrder()->getBillingAddress()->getName();
        $ccExpMonth = $payment->getData('cc_exp_month');
        $ccExpYear = $payment->getData('cc_exp_year');
        $ccCvv = $payment->getData('cc_cid');
        $currency = $payment->getOrder()->getBaseCurrency()->getCode();
        $merchantTnRef = $payment->getOrder()->getIncrementId();
        $comment = "Order " . $payment->getOrder()->getIncrementId();

        $result = array_merge($result, [
            self::CC_NUMBER => $ccNum,
            self::CC_HOLDER => $ccHolder,
            self::CC_EXP_MONTH => $ccExpMonth,
            self::CC_EXP_YEAR => $ccExpYear,
            self::CC_CVV => $ccCvv,
            self::CURRENCY => $currency,
            self::MERCHANT_TN_REF => $merchantTnRef,
            self::COMMENT => $comment
        ]);

        return $result;
    }
}
