<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2018 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentMethod\Gateway\Request;

use Magento\Payment\Gateway\Config\Config;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Payment\Helper\Formatter;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;

/**
 * Class CaptureDataBuilder
 *
 * @package Mdg\PaymentMethod\Gateway\Request
 */
class CaptureDataBuilder extends SaleDataBuilder implements BuilderInterface
{
    const TN_REF = 'tnRef';

    /**
     * @inheritdoc
     */
    public function build(array $subject)
    {
        $result = PaymentDataBuilder::build($subject);

        $paymentDO = $subject['payment'];

        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $payment = $paymentDO->getPayment();
        $tnRef = $payment->getLastTransId();

        if (!$tnRef) {
            return parent::build($subject);
        }

        $result = array_merge($result, [
            self::TN_REF => $tnRef,
        ]);

        return $result;
    }
}
